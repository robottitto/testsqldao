package main.java;

public class Order {

    private int orderId;
    private long total;
    private int userId;
    private String name;
    private String direction;

    public Order(int orderId, long total, int userId, String name, String direction) {
        this.setOrderId(orderId);
        this.setTotal(total);
        this.setUserId(userId);
        this.setName(name);
        this.setDirection(direction);
    }

    public int getOrderId() {
        return this.orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public long getTotal() {
        return this.total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
