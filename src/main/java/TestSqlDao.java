package main.java;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

/**
 * Mejorar cada uno de los métodos a nivel SQL y código cuando sea necesario
 * Razonar cada una de las mejoras que se han implementado
 * No es necesario que el código implementado funcione
 */
public class TestSqlDao {

    private static TestSqlDao instance = new TestSqlDao();
    private Hashtable<Long, Long> maxOrderUser;

    private TestSqlDao() {

    }

    private static TestSqlDao getInstance() {

        return instance;
    }

    /**
     * Obtiene el ID del último pedido para cada usuario
     */
    public Hashtable<Long, Long> getMaxUserOrderId(long idTienda) throws Exception {

        // String query = String.format("SELECT ID_PEDIDO, ID_USUARIO FROM PEDIDOS WHERE ID_TIENDA = %s", idTienda);
        // El principal problema de esta consulta es que tiene que revisar todos y cada uno de los pedidos existentes y comprobar el ID de pedido y el ID de usuario, y si hay muchos pedidos puede ser un proceso muy costoso
        // La consulta se puede optimizar agrupando por el ID_USUARIO y especificando el ID_PEDIDO con MAX, para quedarse con el de mayor ID, que será el último pedido realizado
        String query = String.format("SELECT ID_USUARIO, MAX(ID_PEDIDO) FROM PEDIDOS WHERE ID_TIENDA = %s GROUP_BY ID_USUARIO ORDER_BY ID_USUARIO" , idTienda);
        Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        maxOrderUser = new Hashtable<Long, Long>();

        if (rs.first()) {
            long idPedido = rs.getInt("ID_PEDIDO");
            long idUsuario = rs.getInt("ID_USUARIO");
            maxOrderUser.put(idUsuario, idPedido);
            // Ya no necesitamos comprobar si el idPedido es menor que el guardado con anterioridad, sólo habrá un idPedido por cada idUsuario
            /* if (!maxOrderUser.containsKey(idUsuario)) {
                maxOrderUser.put(idUsuario, idPedido);
            } else if (maxOrderUser.get(idUsuario) < idPedido) {
                maxOrderUser.put(idUsuario, idPedido);
            }*/
        }

        return maxOrderUser;
    }

    /**
     * Copia todos los pedidos de un usuario a otro
     */
    public void copyUserOrders(long idUserOri, long idUserDes) throws Exception {

        // String sql = "INSERT INTO PEDIDOS (ID_USUARIO, FECHA, TOTAL, SUBTOTAL, DIRECCION)
        // SELECT %d, FECHA, TOTAL, SUBTOTAL, DIRECCION FROM PEDIDOS WHERE ID_USUARIO = %s";
        String query = String.format(sql, idUserOri, idUserDes);

        String query = String.format("SELECT FECHA, TOTAL, SUBTOTAL, DIRECCION FROM PEDIDOS WHERE ID_USUARIO = %s", idUserOri);
        Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();

        while (rs.next()) {
            // En el INSERT falta añadir el ID del usuario destino (iduserDes)
            // String insert = String.format("INSERT INTO PEDIDOS (FECHA, TOTAL, SUBTOTAL, DIRECCION) VALUES (%s, %s, %s, %s)",
            String insert = String.format("INSERT INTO PEDIDOS (ID_USUARIO, FECHA, TOTAL, SUBTOTAL, DIRECCION) VALUES (%s, %s, %s, %s, %s)",
                idUserDes,
                rs.getTimestamp("FECHA"),
                rs.getDouble("TOTAL"),
                rs.getDouble("SUBTOTAL"),
                rs.getString("DIRECCION"));

            Connection connection2 = getConnection();
            connection2.setAutoCommit(false);
            PreparedStatement stmt2 = connection2.prepareStatement(insert);
            stmt2.executeUpdate();
            connection2.commit();
        }
    }

    /**
     * Obtiene los datos del usuario y pedido con el pedido de mayor importe para la tienda dada
     */
    // public void getUserMaxOrder(long idTienda, long userId, long orderId, String name, String address) throws Exception {
    // Este método recibe parámetros innecesarios para su funcionalidad, sólo es preciso el idTienda
    public Order getUserMaxOrder(long idTienda) throws Exception {
        // String query = String.format("SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P " + "INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO WHERE P.ID_TIENDA = %", idTienda);
        // La consulta no está seleccionando el pedido de mayor importe

        // Opción 1
        String query = String.format("SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P "
                + "INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO WHERE P.ID_TIENDA = % order by p.total desc", idTienda);

        // Opción 2
        String query = String.format("SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P "
                + "INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO WHERE P.ID_TIENDA = % "
                + " and P.TOTAL = (SELECT MAX(P2.TOTAL) FROM PEDIDOS AS P2 WHERE P2.ID.TIENDA = P.ID_TIENDA)", idTienda);

        Connection connection = getConnection();
        PreparedStatement stmt = connection.prepareStatement(query);
        ResultSet rs = stmt.executeQuery();
        // double total = 0;
		
		// Usamos un objeto de la entidad Order (Pedido) para guardar los datos de la consulta y lo retornamos
        Order order = null;
        
        if (rs.first()) {
            order = new Order(rs.getInt("ID_PEDIDO"), rs.getLong("TOTAL"), rs.getInt("ID_USUARIO"), rs.getString("NOMBRE"), rs.getString("DIRECCION"));
            /* if (rs.getLong("TOTAL") > total) {
                total = rs.getLong("TOTAL");
                userId = rs.getInt("ID_USUARIO");
                orderId = rs.getInt("ID_PEDIDO");
                name = rs.getString("NOMBRE");
                address = rs.getString("DIRECCION");
            } */
        }


        return order;
    }

    private Connection getConnection() {

        // return JDBC connection
        return null;
    }
}
